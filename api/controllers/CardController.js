/**
 * CardController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  order: (req, res) => {
    let params = req.allParams();

    if (params.cards) {
      async.eachOf(
        params.cards,
        async (card, index, callback) => {
          await Card.updateOne(card).set({ order: index + 1 });
          callback();
        },
        (err, results) => {
          return res.ok();
        }
      );
    } else {
      return res.badRequest();
    }
  }
};
