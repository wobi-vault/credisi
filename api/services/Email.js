const nodemailer = require("nodemailer");
module.exports = {
  send: (subject, message, attachments, to, callback) => {
    try {
      var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: sails.config.emailCredentials.email,
          pass: sails.config.emailCredentials.password
        }
      });

      // Activar permiso de aplicaciones: https://myaccount.google.com/lesssecureapps
      const mailOptions = {
        from: sails.config.emailCredentials.email, // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        html: message, // plain text body
        attachments: attachments // attachment files
      };
      transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
          console.log("Err: ", err);
          return callback(err);
        }
        console.log("Email sended to: ", to);
        return callback();
      });
    } catch (error) {
      return callback(error);
    }
  },

  template: params => {
    var message = params.products ?
      '<table align="center" cellpadding="0" cellspacing="0" style="width:650px">' +
      "<tbody>" +
      "<tr>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      '<td align="left" style="width:570px; height:140px; background:#0966B2;">' +
      '<img alt="credisi-logo" src="/images/credisi.png" width="150" />' +
      "</td>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "<tr>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      '<td style="width:570px;background:#fff;color:#565656; padding:20px 30px; font-size:14px">' +
      '<table align="center" cellpadding="0" cellspacing="0" style="width:500px">' +
      "<tbody>" +
      '<tr style="color: #000;">' +
      '<td style="padding:10px;">Este es un correo enviado desde <strong><span style="color:#0966B2;">Credisi</span>' +
      '<span style="color:#F15B33">.</span><span style="color:#0966B2;">mx</span> </strong></td>' +
      "</tr>" +
      '<tr style="color: #000;">' +
      '<td style="padding:10px;"><strong>Nombre: </strong>' +
      params.name +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Teléfono: </strong>' +
      params.phone +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Email: </strong>' +
      params.email +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Productos: </strong>' +
      params.products.join(", ") +
      "</td>" +
      "</tr>" +
      "</tbody>" +
      "</table>" +
      "</td>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "<tr>" +
      '<td style="width:40px; background:#0966B2;">&nbsp;</td>' +
      '<td align="center" style="width:570px;height:90px; background:#0966B2; padding:0 30px; font-size:14px; color: #fff; ">Credisi 2019</td>' +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "</tbody>" +
      "</table>" :
      '<table align="center" cellpadding="0" cellspacing="0" style="width:650px">' +
      "<tbody>" +
      "<tr>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      '<td align="left" style="width:570px; height:140px; background:#0966B2;">' +
      '<img alt="credisi-logo" src="/images/credisi.png" width="150" />' +
      "</td>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "<tr>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      '<td style="width:570px;background:#fff;color:#565656; padding:20px 30px; font-size:14px">' +
      '<table align="center" cellpadding="0" cellspacing="0" style="width:500px">' +
      "<tbody>" +
      '<tr style="color: #000;">' +
      '<td style="padding:10px;">Este es un correo enviado desde <strong><span style="color:#0966B2;">Credisi</span>' +
      '<span style="color:#F15B33">.</span><span style="color:#0966B2;">mx</span> </strong></td>' +
      "</tr>" +
      '<tr style="color: #000;">' +
      '<td style="padding:10px;"><strong>Nombre: </strong>' +
      params.name +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Teléfono: </strong>' +
      params.phone +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Email: </strong>' +
      params.email +
      "</td>" +
      "</tr>" +
      '<tr style="color: #000">' +
      '<td style="padding:10px;"><strong>Mensaje: </strong>' +
      (params.message || "Sin mensaje") +
      "</td>" +
      "</tr>" +
      "</tbody>" +
      "</table>" +
      "</td>" +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "<tr>" +
      '<td style="width:40px; background:#0966B2;">&nbsp;</td>' +
      '<td align="center" style="width:570px;height:90px; background:#0966B2; padding:0 30px; font-size:14px; color: #fff; ">Credisi 2019</td>' +
      '<td style="width:40px;background:#0966B2;">&nbsp;</td>' +
      "</tr>" +
      "</tbody>" +
      "</table>";
    return message;
  }
};