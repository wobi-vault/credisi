'use strict';
angular
  .module('app', [])
  .directive('limitTo', [
    function() {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var limit = parseInt(attrs.limitTo);
          angular.element(elem).on('keypress', function(e) {
            var key;
            if (e.which == null) {
              // IE
              key = e.keyCode;
            }
            if (e.which != 0) {
              // all but IE
              key = e.which;
            }
            if (
              this.value.length == limit &&
              (key != 8 && key !== 46 && key !== undefined)
            ) {
              e.preventDefault();
            }
          });
        }
      };
    }
  ])
  .controller('home', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Credisi home');

      $scope.setOrigin = function(origin) {
        sessionStorage.clear();
        sessionStorage.setItem('origin', origin);
      };
    }
  ])
  .controller('step1', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Credisi step1');

      $scope.onlyNumbers = /^\d+$/;
      $scope.emailExpression = /^\w+([\.\+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
      $scope.loading = true;

      $scope.f = {
        email: '',
        phone: '',
        phonebrand: null,
        origin: sessionStorage.getItem('origin')
      };

      $http.get('/phonebrand?sort=description').then(
        res => {
          $scope.phonebrand = res.data;
          $scope.loading = false;
        },
        err => {
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          $scope.loading = true;
          $http.post('/lead', $scope.f).then(
            res => {
              sessionStorage.setItem('leadId', res.data.id);
              sessionStorage.setItem('phonebrandId', $scope.f.phonebrand);
              window.location = '../step2';
            },
            err => {
              console.error(err);
            }
          );
        }
      };
    }
  ])
  .controller('step2', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Credisi step2');

      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      }

      $scope.loading = true;
      $scope.f = {
        income: '',
        checkIncome: '',
        lead: sessionStorage.getItem('leadId')
      };

      $http.get('/income?sort=id').then(
        res => {
          $scope.income = res.data;
          $scope.loading = false;
        },
        err => {
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        console.log('register');
        // Formulario válido
        if (isValid) {
          sessionStorage.setItem('income', $scope.f.income);
          sessionStorage.setItem('checkIncome', $scope.f.checkIncome);
          $scope.loading = true;
          if (sessionStorage.getItem('applicationId')) {
            $http
              .patch(
                '/application/' + sessionStorage.getItem('applicationId'),
                $scope.f
              )
              .then(
                res => {
                  window.location = '../step3';
                },
                err => {
                  console.error(err);
                }
              );
          } else {
            $http.post('/application', $scope.f).then(
              res => {
                sessionStorage.setItem('applicationId', res.data.id);
                window.location = '../step3';
              },
              err => {
                console.error(err);
              }
            );
          }
        }
      };
    }
  ])
  .controller('step3', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Credisi step3');

      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      } else if (!sessionStorage.getItem('applicationId')) {
        window.location = '../step2';
      }

      $scope.loading = true;
      $scope.f = {
        profession: ''
      };

      $http.get('/profession?sort=id').then(
        res => {
          $scope.profession = res.data;
          $scope.loading = false;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          sessionStorage.setItem('profession', $scope.f.profession);
          $scope.loading = true;
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                window.location = '../step4';
              },
              err => {
                console.error(err);
              }
            );
        }
      };
    }
  ])
  .controller('step4', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Credisi step4');

      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      } else if (!sessionStorage.getItem('applicationId')) {
        window.location = '../step2';
      }

      $scope.f = {
        debit: ''
      };

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          sessionStorage.setItem('debit', $scope.f.debit);
          $scope.loading = true;
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                window.location = '../step5';
              },
              err => {
                console.error(err);
              }
            );
        }
      };
    }
  ])
  .controller('step5', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Credisi step5');

      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      } else if (!sessionStorage.getItem('applicationId')) {
        window.location = '../step2';
      }

      $scope.loading = true;
      $scope.f = {
        paymentUpToDate: null,
        financialProduct: []
      };

      $http.get('/financialProduct?sort=id').then(
        res => {
          $scope.financialProduct = res.data;
          $scope.loading = false;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $scope.change = function(id) {
        var index = $scope.f.financialProduct.indexOf(id);
        if (index >= 0) {
          $scope.f.financialProduct.splice(index, 1);
        } else {
          $scope.f.financialProduct.push(id);
        }
        $scope.f.paymentUpToDate =
          $scope.f.financialProduct.length > 0 ?
          $scope.f.paymentUpToDate :
          null;
      };

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          $scope.loading = true;
          $http
            .patch(
              '/application/' + sessionStorage.getItem('applicationId'),
              $scope.f
            )
            .then(
              res => {
                window.location = '../step6';
              },
              err => {
                console.error(err);
              }
            );
        }
      };
    }
  ])
  .controller('step6', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Credisi step6');

      $scope.loading = true;
      if (!sessionStorage.getItem('leadId')) {
        window.location = '../step1';
      } else if (!sessionStorage.getItem('applicationId')) {
        window.location = '../step2';
      }

      var date = new Date();
      date.setFullYear(date.getFullYear() - 18);
      date.setMonth(0);
      date.setDate(1);

      $scope.months = [
        'ENE',
        'FEB',
        'MAR',
        'ABR',
        'MAY',
        'JUN',
        'JUL',
        'AGO',
        'SEP',
        'OCT',
        'NOV',
        'DIC'
      ];

      $scope.f = {
        dayBirth: 1,
        monthBirth: 'ENE',
        yearBirth: date.getFullYear(),
        state: null,
        termsConditions: null,
        privacyNotice: null
      };

      $scope.daysInMonth = function() {
        var month = $scope.months.indexOf($scope.f.monthBirth) + 1;
        $scope.days = $scope.range(
          1,
          new Date($scope.f.yearBirth, month, 0).getDate(),
          1
        );
        console.log($scope.f);
      };

      $scope.range = function(min, max, step) {
        step = Math.abs(step) || 1;
        var input = [];
        var range = Math.abs(max - min) + 1;
        for (var i = 0; i < range; i++) {
          input.push(min);
          min = max - min >= 0 ? min + step : min - step;
        }
        return input;
      };

      $scope.years = $scope.range(
        date.getFullYear(),
        date.getFullYear() - 82,
        1
      );
      $scope.daysInMonth();

      $http.get('/state?sort=description').then(
        res => {
          $scope.state = res.data;
          $scope.f.state = $scope.state[0].id;
          $scope.loading = false;
        },
        err => {
          // Sweetalert
          console.error(err);
        }
      );

      $scope.register = function(isValid) {
        // Formulario válido
        if (isValid) {
          $scope.loading = true;
          $http
            .patch('/lead/' + sessionStorage.getItem('leadId'), $scope.f)
            .then(
              res => {
                $http
                  .patch(
                    '/application/' + sessionStorage.getItem('applicationId'),
                    $scope.f
                  )
                  .then(
                    res => {
                      fbq('track', 'Lead');
                      window.location = '../results';
                    },
                    err => {
                      console.error(err);
                    }
                  );
              },
              err => {
                console.error(err);
              }
            );
        }
      };
    }
  ])
  .controller('email', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;

      console.log('Credisi email');

      $scope.lettersForNames = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ-\s]+$/;
      $scope.onlyNumbers = /^\d+$/;
      $scope.emailExpression = /^\w+([\.\+-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

      $scope.f = {};

      $scope.change = function(prod) {
        $scope.f.products = $scope.f.products ? $scope.f.products : [];
        var index = $scope.f.products.indexOf(prod);
        if (index >= 0) {
          $scope.f.products.splice(index, 1);
        } else {
          $scope.f.products.push(prod);
        }
        console.log($scope.f.products);
      };

      $scope.sendEmail = function(isValid) {
        // Formulario válido
        if (isValid && !$scope.sending) {
          $scope.sending = true;
          $http.post('/email', $scope.f).then(
            res => {
              $scope.sending = false;
              window.location.reload();
            },
            err => {
              $scope.sending = false;
              console.error(err);
            }
          );
        }
      };
    }
  ])
  .controller('results', [
    '$scope',
    '$http',
    function($scope, $http) {
      var vm = this;
      const phoneBrandIds = [1, 2]; // Samsung, LG
      const Over1M = 2; // $1,000,000 >
      const Over4_5M = 4; // $4,500,00 >
      const Unemployed = 3; // Desempleado
      const LineruId = 1; // Lineru
      const CrediminutoId = 2; // Crediminuto
      const RapicreditId = 3; // Rapicredit
      const PrestaenlineaId = 4; // Presta en linea

      var currentPhoneBrandId = sessionStorage.getItem('phonebrandId');
      var currentIncomeId = sessionStorage.getItem('income');
      var currentcheckIncome = sessionStorage.getItem('checkIncome');
      var currentProfessionId = sessionStorage.getItem('profession');
      var currentDebit = sessionStorage.getItem('debit');

      if (currentPhoneBrandId) currentPhoneBrandId = parseInt(currentPhoneBrandId);
      if (currentIncomeId) currentIncomeId = parseInt(currentIncomeId);
      if (currentcheckIncome) currentcheckIncome = (currentcheckIncome == 'true' || currentcheckIncome == '1') ? true : false;
      if (currentProfessionId) currentProfessionId = parseInt(currentProfessionId);
      if (currentDebit) currentDebit = (currentDebit == 'true' || currentDebit == '1') ? true : false;

      $scope.loading = true;
      console.log('Credisi results');

      $http.get('/card?sort=order&active=1').then(
        res => {
          $scope.cards = res.data;
          // Crediminuto rules (Phone brand id must be 1 or 2)
          if (!phoneBrandIds.includes(currentPhoneBrandId)) {
            $scope.cards = $scope.cards.filter(c => {
              return CrediminutoId != c.id;
            });
          }

          // PrestaEnLinea rules (Income under 1M and over 4.5M)
          if (currentIncomeId >= Over1M && currentIncomeId <= Over4_5M) {
            $scope.cards = $scope.cards.filter(c => {
              return PrestaenlineaId != c.id;
            });
          }

          // Lineru has not rules
          // Rapicredit has not rules

          $scope.cards.forEach((c, i) => {
            c.description = c.description.split(String.fromCharCode(10));
          });
          $scope.loading = false;
        },
        err => {
          console.error(err);
        }
      );
    }
  ])
  .controller('pages', [
    '$scope',
    '$http',
    '$sce',
    '$location',
    function($scope, $http, $sce, $location) {
      var vm = this;
      var path = $location.absUrl().split('/')[3];
      var idPage = 0;

      switch (path) {
        case 'loan':
          idPage = 1;
          break;
        case 'healers':
          idPage = 2;
          break;
        case 'terms':
          idPage = 3;
          break;
        case 'privacy':
          idPage = 4;
          break;
        case 'faq':
          idPage = 5;
          break;
        case 'about':
          idPage = 6;
          break;
      }

      $http.get('/page/' + idPage).then(
        res => {
          $scope.page = res.data;
          $scope.page.htmlContent = $sce.trustAsHtml($scope.page.htmlContent);
        },
        err => {
          console.error(err);
        }
      );
    }
  ]);